public class operator_logic{
	public static void main(String args[]){
	// Operator Logika --> operasi yang bisa kita lakukan kepada tipe data boolean
	// And, OR, XOR, negasi
	
	boolean a,b,c;
	
	//logic OR (||) analoginya seperti perhitungan penjumlahan
	//false = 0 dan true = 1
	//maka apabila 0 + 0 = 0/false dan apabila 1 + 1 = true
	System.out.println("===== OR (||) =====\n");
	a = false;
	b = false;
	c = (a||b);
	System.out.println(a + "||" + b +"="+ c +"\n");
	a = true;
	b = false;
	c = (a||b);
	System.out.println(a + "||" + b +"="+ c +"\n");
	a = false;
	b = true;
	c = (a||b);
	System.out.println(a + "||" + b +"="+ c +"\n");
	a = true;
	b = true;
	c = (a||b);
	System.out.println(a + "||" + b +"="+ c +"\n\n");
    	
	//logic AND (&&) analoginya seperti perhitungan pembagian apabila
	//false = 0 dan true = 1
	//maka apabila 0 : 0 = 0/false dan apabila 1:1 = true
	System.out.println("===== AND (&&) =====");
	a = false;
	b = false;
	c = (a&&b);
	System.out.println(a + "&&" + b +"="+ c +"\n");
	a = true;
	b = false;
	c = (a&&b);
        System.out.println(a + "&&" + b +"="+ c +"\n");
	a = false;
	b = true;
	c = (a&&b);
	System.out.println(a + "&&" + b +"="+ c +"\n");
	a = true;
	b = true;
	c = (a&&b);
	System.out.println(a + "&&" + b +"="+ c +"\n");
	
	//logic XOR(^) apabila dua duanya benar atau salah maka hasilnya akan false
	System.out.println("===== XOR(^) =====");
	a = false;
	b = false;
	c = (a^b);
	System.out.println(a + "^" + b +"="+ c +"\n");
	a = false;
	b = true;
	c = (a^b);
	System.out.println(a + "^" + b +"="+ c +"\n");
	a = true;
	b = false;
	c = (a^b);
	System.out.println(a + "^" + b +"="+ c +"\n");
	a = true;
	b = true;
	c = (a^b);
	System.out.println(a + "^" + b +"="+ c +"\n");

	//logic NOR (!) atau kebalikan
	System.out.println("===== NOT(!) =====");
	a = false;
	c = !a;
	System.out.println(a + "--> (!) = " + c);

	a = true;
	c = !a;
	System.out.println(a + "--> (!) = " + c);

	}
}
