class SetterAndGetter{
    private int Id;
    private String NIP;
    private String Name;
    private String Address;
    private String Fakultas;
    private String kelas;

    public void setId(int Id){
        this.Id = Id;
    }

    public int getId(){
      return this.Id;  
    }

    public void setNIP(String NIP){
        this.NIP = NIP;
    }

    public String getNIP(){
        return this.NIP;
    }

    public void setName(String Name){
        this.Name = Name;
    }

    public String getName(){
        return this.Name;
    }

    public void setAddress(String Address){
        this.Address = Address;
    }

    public String getAddress(){
        return this.Address;
    }

    public void setFakultas(String Fakultas){
        this.Fakultas = Fakultas;
    }

    public String getFakultas(){
        return this.Fakultas;
    }

    public void setKelas(String kelas){
        this.kelas = kelas;
    }

    public String getKelas(){
        return this.kelas;
    }



}