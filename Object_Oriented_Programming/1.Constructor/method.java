public class method {
    public String first_name;
    public String last_name;
    public int age;
    public String address;
    public String number_phone;

    // 1).method constructor
        // public method(){
        //        System.out.println("say hey"); 
        // }

    // 2). method constructor with parameter
        // public method(String first_name, String last_name, int age, String address, String number_phone){
        //     this.first_name = first_name;
        //     this.last_name = last_name;
        //     this.age = age;
        //     this.address = address;
        //     this.number_phone = number_phone;
        // }

    // 3). constructor overloading in java multiple constructor for a java class 
        // description : overloading atau bisa kita sebut dengan "memiliki beberapa contoh dari hal yang sama"
        // constructor overloading di java adalah proses yang memiliki lebih dari satu 
        // constructor dengan daftar parameter berbeda.
    public method(String name){
        System.out.println("constructor with one parameter : ");
        System.out.println("name = " + name);
    }

    public method(String name, int age){
        System.out.println("constructor with two parameter : ");
        System.out.println("name = " + name);
        System.out.println("age = " + age);
    }

    
            

}
