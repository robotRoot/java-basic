public class main{
    public static void main(String args[]){
        int i = 10;
        System.out.println("===========INTEGER===========");
        System.out.println("Integer Value i ="+ (i+1));
        System.out.println("Max Value = " + Integer.MAX_VALUE);
        System.out.println("Min Value = " + Integer.MIN_VALUE);
        System.out.println("Bytes Integer = "+ Integer.BYTES + "bytes");
        System.out.println("Size Integer ="+ Integer.SIZE + "bit");

        byte b = 10;
        System.out.println("===========BYTE===========");
        System.out.println("Byte Value b ="+ b);
        System.out.println("Max Value = " + Byte.MAX_VALUE);
        System.out.println("Min Value = " + Byte.MIN_VALUE);
        System.out.println("Bytes Integer = "+ Byte.BYTES + "bytes");
        System.out.println("Size Integer ="+ Byte.SIZE + "bit");

        short s = 10;
        System.out.println("===========SHORT===========");
        System.out.println("Short Value s ="+ s);
        System.out.println("Max Value = " + Short.MAX_VALUE);
        System.out.println("Min Value = " + Short.MIN_VALUE);
        System.out.println("Bytes Integer = "+ Short.BYTES + "bytes");
        System.out.println("Size Integer ="+ Short.SIZE + "bit");

        long l = 10L;
        System.out.println("===========LONG===========");
        System.out.println("Long Value s ="+ l);
        System.out.println("Max Value = " + Long.MAX_VALUE);
        System.out.println("Min Value = " + Long.MIN_VALUE);
        System.out.println("Bytes Integer = "+ Long.BYTES + "bytes");
        System.out.println("Size Integer ="+ Long.SIZE + "bit");

        double d = 10.5d;
        System.out.println("===========DOUBLE===========");
        System.out.println("Double Value s ="+ d);
        System.out.println("Max Value = " + Double.MAX_VALUE);
        System.out.println("Min Value = " + Double.MIN_VALUE);
        System.out.println("Bytes Integer = "+ Double.BYTES + "bytes");
        System.out.println("Size Integer ="+ Double.SIZE + "bit");

        float f = -8.5f;
        System.out.println("===========FLOAT===========");
        System.out.println("Float Value s ="+ f);
        System.out.println("Max Value = " + Float.MAX_VALUE);
        System.out.println("Min Value = " + Float.MIN_VALUE);
        System.out.println("Bytes Integer = "+ Float.BYTES + "bytes");
        System.out.println("Size Integer ="+ Float.SIZE + "bit");

        char c = 'c';
        System.out.println("===========CHAR===========");
        System.out.println("Char Value s ="+ c);
        System.out.println("Max Value = " + Float.MAX_VALUE);
        System.out.println("Min Value = " + Float.MIN_VALUE);
        System.out.println("Bytes Integer = "+ Float.BYTES + "bytes");
        System.out.println("Size Integer ="+ Float.SIZE + "bit");

        boolean val = false;
        System.out.println("===========CHAR===========");
        System.out.println("Boolean Value s =" + val);
        System.out.println("Max Value = " + Boolean.TRUE);
        System.out.println("Min Value = " + Boolean.FALSE);


    }
}