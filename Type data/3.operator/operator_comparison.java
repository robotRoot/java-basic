public class operator_comparison{
    public static void main(String args[]){
        
        // =========================> Operator Komparasi
        // operator komperasi beralaku untuk semua nilai
        /**
         * printf is print to float
         * %s yang akan menampung nilai hasilKomparasi 
         * %s yang akan menampung nilai a dan b
         */

        int a,b;
        boolean hasilKomparasi;

        //================ > operator equal atau sama dengan
        // resul is true
        System.out.println("------------------------ Persamaan");
        a = 10;
        b = 10;
        
        hasilKomparasi = (a == b);
        System.out.printf("%d apakah sama dengan  %d ? %s\n",a,b,hasilKomparasi);

        a = 12;
        b = 10;
        // resul is false
        hasilKomparasi = (a == b);
        System.out.printf("%d apakah sama dengan  %d ? %s\n\n",a,b,hasilKomparasi);

        //================ > operator not equal atau tidak sama dengan
        // resul is true
        System.out.println("------------------------ Pertidaksamaan");
        a = 10;
        b = 10;
        
        hasilKomparasi = (a != b);
        System.out.printf("%d apakah sama dengan  %d ? %s\n",a,b,hasilKomparasi);

        a = 12;
        b = 10;
        // resul is false
        hasilKomparasi = (a != b);
        System.out.printf("%d apakah sama dengan  %d ? %s\n\n",a,b,hasilKomparasi);

        //================ > less than atau kurang dari
        // resul is true
        System.out.println("------------------------ kurang dari");
        a = 9;
        b = 10;
        
        hasilKomparasi = (a < b);
        System.out.printf("%d apakah sama dengan  %d ? %s\n",a,b,hasilKomparasi);

        a = 12;
        b = 10;
        // resul is false
        hasilKomparasi = (a < b);
        System.out.printf("%d apakah sama dengan  %d ? %s\n\n",a,b,hasilKomparasi);

        //================ > operator greater than atau lebih dari
        // resul is true
        System.out.println("------------------------ lebih dari");
        a = 9;
        b = 10;
        
        hasilKomparasi = (a > b);
        System.out.printf("%d >  %d ? %s\n",a,b,hasilKomparasi);

         //================ > less than atau kurang dari 
        // resul is true
        System.out.println("------------------------ kurang dari");
        a = 9;
        b = 10;
        
        hasilKomparasi = (a < b);
        System.out.printf("%d <  %d ? %s\n",a,b,hasilKomparasi);

        //================ > operator greater than equal atau lebih dari sama dengan
        // resul is true
        System.out.println("------------------------ lebih dari sama dengan");
        a = 9;
        b = 10;
        
        hasilKomparasi = (a >= b);
        System.out.printf("%d >=  %d ? %s\n",a,b,hasilKomparasi);

        System.out.println("------------------------ kurang dari sama dengan");
        //================ > operator less than equal atau lebih dari sama dengan
        a = 9;
        b = 10;
        // resul is false
        hasilKomparasi = (a <= b);
        System.out.printf("%d <=  %d ? %s\n\n",a,b,hasilKomparasi);

    }
}
