package ListStringProgramBilangan;

import java.util.*;

public class ListringProgramBilangan {
	public static void main(String args[]) {
		Boolean running = true;
		int count = 0;
		while(running) {
			   System.out.println("================ Program Jenis Bilangan Math ================");
               System.out.println ("Masukan nomor pilihan :");
               System.out.println ("Bilangan Genap : 1");
               System.out.println("Bilangan Ganjil : 2");
               System.out.println ("Bilangan Prima : 3");
               System.out.println ("Bilangan Fibonacci : 4");
               System.out.println("Exit : 5");
               System.out.print ("Masukan Nomor = ");
               Scanner scan = new Scanner(System.in);
               int select = scan.nextInt();
               if(select == 1) {
    			   System.out.println("================ Bilangan Genap ================");
            	   System.out.print("Masukan Bilangan genap : ");
            	   System.out.print("\n");
                   int inp1 = scan.nextInt();
            	   int n1 = inp1 * 2;
            	   for(int i = 1; i<n1; i++) {
            		   if(i%2 == 0) {
            			   System.out.print(i + "\n");
            		   }
            	   }
               }else if(select == 2) {
    			   System.out.println("================ Bilangan Ganjil ================");
            	   System.out.print("Masukan Bilangan ganjil : ");
            	   System.out.print("\n");
                   int inp2 = scan.nextInt();
            	   int n2 = (inp2 * 2) - 1;
            	   for(int j = 1; j<n2; j++) {
            		   if(j%2 != 0) {
            			   System.out.print(j + "\n");
            		   }
            	   }
               }else if(select == 3) {
    			   System.out.println("================ Bilangan Prima ================");
            	 System.out.print("Masukan bilangan prima : ");
                 int inp3 = scan.nextInt();
                 int n3 = (inp3 * 2) + 2;
            	 	for(int k =1; k<n3; k++) {
                    	if(k % 2 == 0) {
                    		System.out.print(k);
                    		for(int l = 1; l < n3; l++) {
                    			if(l % 2 != 0) {
                    				System.out.print(l);
                    			}
                    		}
                    		break;
                    	}
                    }
               System.out.println("\n");
            }else if(select == 4) {
  			   System.out.println("================ Bilangan Fibonacci ================");
  			   System.out.println("Masukan bilangan Fibonacci : "); 
// ========= first formula	   
//            	int inp4 = scan.nextInt();
// 			   	int numb1 = 1;
// 			   	int numb2 = 1;
// 			   	for(int m = 1; m < inp4; m++) {
// 				   System.out.print(numb2 + "+"); 
// 				   		int sum = numb2 + numb1;
// 				   	numb2 = numb1;
// 				   	numb1 = sum;
// 			   }
  			   
// ========= second formula 
  			   int past = 1;
  			   int current = 1;
  			   int fibonacci = 1;
  			   int inptLimit = scan.nextInt();
  			   for(int i = 1; i <= inptLimit; i++) {
  				   System.out.print(current + "+");
  				   fibonacci = past + current;
  				   past = current;
  				   current = fibonacci;
  			   }
  			   
 			   	System.out.println("");
            }
		}
	}
}
