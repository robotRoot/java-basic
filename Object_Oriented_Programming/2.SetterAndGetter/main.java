public class main{
	public static void main(String args[]){
		SetterAndGetter setterAndGetter = new SetterAndGetter();
		
		setterAndGetter.setId(1);
		setterAndGetter.setNIP("201643579078");
		setterAndGetter.setName("Acep Munawar");
		setterAndGetter.setAddress("Kota Bekasi");
		setterAndGetter.setKelas("Ax7");

		System.out.println("======= BIO College Student =======\n");
		System.out.print("Id		:" + setterAndGetter.getId()+"\n");
		System.out.print("NIP		:" + setterAndGetter.getNIP()+"\n");
		System.out.print("Name		:" + setterAndGetter.getName()+"\n");
		System.out.print("Address	\t:" + setterAndGetter.getAddress()+"\n");
		System.out.print("Kelas		:" + setterAndGetter.getKelas()+"\n");
	}
}
